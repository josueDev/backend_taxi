package com.codename1.uberclone.webpage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Home {
        @RequestMapping("/home.html")
        public String index() {
            return "home";
        }
}